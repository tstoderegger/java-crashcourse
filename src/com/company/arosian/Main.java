package com.company.arosian;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        int age = 20;
        boolean isAwake = true;
        double height = 50.12;
        float x = 45L;

        String name = "eurofunk";
        Integer age2 = 20;
        Boolean isAwake2 = false;
        Double height2 = 50.12;


        List<String> strings = new ArrayList<>();
        Optional<String> value = strings.stream().findFirst();
        if (value.isPresent()) {
            System.out.println(value);
        }
    }

    public List<String> getUsernamesWithLoop(List<User> users) {
        List<String> usernames = new ArrayList<>();
        for (User user : users) {
            usernames.add(user.getUsername());
        }
        return usernames;
    }

    public List<String> getUsernamesWithStream(List<User> users) {
        return users.stream().map(User::getUsername).collect(Collectors.toList());
    }

    public Integer sumOfAllAgesWithStream(List<User> users) {
        return users.stream().mapToInt(User::getAge).sum();
    }

    public Boolean anyMatchWithStream(List<User> users, Integer age) {
        return users.stream().anyMatch(User::isAgeLowerThan30);
    }

    public Boolean noneMatchWithStream(List<User> users, Integer age) {
        return users.stream().noneMatch(User::isAgeLowerThan30);
    }

    public List<User> filterUsersByAgeWithFunctionReference(List<User> users) {
        return users.stream().filter(User::isAgeLowerThan30).collect(Collectors.toList());
    }

    public List<User> filterUsersByAgeWithLambda(List<User> users) {
        return users.stream().filter(user -> user.isAgeLowerThan30()).collect(Collectors.toList());
    }

    public List<Integer> filterOddNumbersWithStream(List<Integer> numbers) {
        return numbers.stream().filter(number -> (number & 1) == 0).collect(Collectors.toList());
    }

    public List<Integer> filterOddNumbersWithFunction(List<Integer> numbers) {
        List<Integer> oddNumbers = new ArrayList<>();
        for (Integer number : numbers) {
            if ((number & 1) == 0) {
                oddNumbers.add(number);
            }
        }
        return oddNumbers;
    }

    public Boolean ageLessThanNumber(List<User> users, Integer number) {
        return users.stream().anyMatch(user -> user.getAge() < number);
    }

}
