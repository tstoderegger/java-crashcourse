package com.company.arosian;

import org.junit.jupiter.api.Assertions;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class Test {

    private Main main = new Main();

    private List<User> users = Arrays.asList(new User("Tobi", 35), new User("Andi", 16), new User("Markus", 8));

    @org.junit.jupiter.api.Test
    void getUsernamesWithLoop() {
        main.getUsernamesWithLoop(this.users);
        Assertions.assertEquals(this.users.size(), this.main.getUsernamesWithLoop(this.users).size());
    }

    @org.junit.jupiter.api.Test
    void getUsernamesWithStream() {
        Assertions.assertEquals(this.users.size(), this.main.getUsernamesWithStream(this.users).size());
    }

    @org.junit.jupiter.api.Test
    void sumOfAllAgesWithStream() {
        Assertions.assertEquals(59, this.main.sumOfAllAgesWithStream(this.users));
    }

    @org.junit.jupiter.api.Test
    void anyMatchWithStream() {
        Assertions.assertEquals(true, this.main.anyMatchWithStream(this.users, 8));
    }

    @org.junit.jupiter.api.Test
    void noneMatchWithStream() {
        Assertions.assertEquals(true, this.main.anyMatchWithStream(this.users, 8));
    }

    @org.junit.jupiter.api.Test
    void filterUsersByAgeWithFunctionReference() {
        Assertions.assertEquals(2,this.main.filterUsersByAgeWithFunctionReference(this.users).size());
    }

    @org.junit.jupiter.api.Test
    void filterUsersByAgeWithLambda() {
        Assertions.assertEquals(2, this.main.filterUsersByAgeWithFunctionReference(this.users).size() );
    }

    @org.junit.jupiter.api.Test
    void ageLessThanNumber() {
        Assertions.assertEquals(true, this.main.ageLessThanNumber(users, 10));
    }
}